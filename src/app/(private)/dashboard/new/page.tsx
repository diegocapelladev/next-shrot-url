import Link from 'next/link'

import { Input } from '@/components/ui/input'
import { Button } from '@/components/ui/button'
import * as Typography from '@/components/ui/typography'
import database from '@/lib/database'
import { redirect } from 'next/navigation'
import { revalidatePath } from 'next/cache'
import SubmitButton from './submit-button'

export default function NewLink() {
  async function create(formData: FormData) {
    'use server'

    const title = formData.get('title') as string
    const originalUrl = formData.get('originalUrl') as string
    const slug = formData.get('slug') as string

    await database.link.create({
      data: {
        title,
        originalURL: originalUrl,
        slug,
      },
    })

    revalidatePath('/dashboard')
    redirect('/dashboard')
  }

  return (
    <>
      <section className="flex justify-between items-center">
        <Typography.H3>Novo link</Typography.H3>

        <Link href="/dashboard">
          <Button variant="link">Voltar</Button>
        </Link>
      </section>

      <section className="my-6">
        <form action={create} className="space-y-3">
          <Input required type="text" name="title" placeholder="Titulo" />
          <Input
            required
            type="url"
            name="originalUrl"
            placeholder="URL Original"
          />
          <Input required type="text" name="slug" placeholder="Slug" />

          <SubmitButton />
        </form>
      </section>
    </>
  )
}
