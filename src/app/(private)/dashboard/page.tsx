import Link from 'next/link'

import {
  Table,
  TableBody,
  TableCaption,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from '@/components/ui/table'
import { Button } from '@/components/ui/button'
import database from '@/lib/database'
import * as Typography from '@/components/ui/typography'
import DeleteButton from './delete-button'

export default async function Dashboard() {
  const links = await database.link.findMany()

  return (
    <>
      <section className="flex justify-between items-center">
        <Typography.H3>Seus links</Typography.H3>

        <Link href="/dashboard/new">
          <Button className="bg-indigo-600">Adicionar</Button>
        </Link>
      </section>

      <section className="mt-12">
        <Table>
          <TableCaption>Links que você cadastrou.</TableCaption>
          <TableHeader>
            <TableRow>
              <TableHead className="w-[100px]">ID</TableHead>
              <TableHead>Title</TableHead>
              <TableHead>Slug</TableHead>
              <TableHead>Original URL</TableHead>
              <TableHead>Views</TableHead>
              <TableHead>Date</TableHead>
              <TableHead>Actions</TableHead>
            </TableRow>
          </TableHeader>
          <TableBody>
            {links.map((link) => (
              <TableRow key={link.id}>
                <TableCell>{link.id}</TableCell>
                <TableCell>{link.title}</TableCell>
                <TableCell>{link.slug}</TableCell>
                <TableCell>{link.originalURL}</TableCell>
                <TableCell>{link.viewsCount}</TableCell>
                <TableCell>{link.createdAt.toLocaleDateString()}</TableCell>
                <TableCell>
                  <DeleteButton id={link.id} />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </section>
    </>
  )
}
