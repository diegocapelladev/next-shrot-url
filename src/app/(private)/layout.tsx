import { UserButton } from '@clerk/nextjs'
import * as Typography from '@/components/ui/typography'
import Link from 'next/link'

export default function PrivateLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <>
      <nav className="flex justify-between mx-24 mt-6 items-center">
        <Link href="/dashboard">
          <Typography.H3>Next Short</Typography.H3>
        </Link>

        <div>
          <UserButton afterSignOutUrl="/" />
        </div>
      </nav>

      <section className="rounded-lg shadow-2xl px-24 py-12 mx-3 my-12 h-3/5">
        {children}
      </section>
    </>
  )
}
